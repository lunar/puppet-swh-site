---
backups::exclude:
  - annex
  - data
  - mnt
  - srv/softwareheritage/annex
  - srv/softwareheritage/objects-xfs

# Deploy the storage server as a public resource
swh::deploy::storage::backend::listen::host: 0.0.0.0
swh::deploy::storage::backend::workers: 128
swh::deploy::storage::backend::max_requests: 5000
swh::deploy::storage::backend::max_requests_jitter: 500

swh::deploy::storage::legacy_directory: /srv/softwareheritage/objects-xfs

swh::deploy::storage::config::local:
  cls: postgresql
  db: "host=%{hiera('swh::deploy::storage::db::host')} user=%{hiera('swh::deploy::storage::db::user')} dbname=%{hiera('swh::deploy::storage::db::dbname')} password=%{hiera('swh::deploy::storage::db::password')}"
  journal_writer: "%{alias('swh::deploy::journal::writer::config')}"
  objstorage:
    cls: multiplexer
    objstorages:
      - cls: pathslicing
        root: "%{hiera('swh::deploy::storage::directory')}"
        slicing: "0:2/0:5"
        compression: none
      - "%{alias('swh::remote_service::objstorage::config::azure')}"

# Deploy the indexer storage server as a public resource
swh::deploy::indexer::storage::backend::listen::host: 0.0.0.0
swh::deploy::indexer::storage::backend::workers: 32

# open objstorage api
swh::deploy::objstorage::backend::listen::host: 0.0.0.0
swh::deploy::objstorage::backend::workers: 16
swh::deploy::objstorage::config:
  objstorage:
    cls: multiplexer
    objstorages:
      - cls: pathslicing
        root: "%{hiera('swh::deploy::storage::directory')}"
        slicing: "0:2/0:5"
        compression: none
  client_max_size: 1073741824  # 1 GiB

icinga2::host::vars:
  load: high

nginx::worker_processes: 32

swh::apt_config::enable_non_free: true
packages:
  - intel-microcode

mountpoints:
  # override default mountpoints
  /srv/softwareheritage/objects:
    # zfs mount: not in fstab
    ensure: absent
  /tmp:
    # Don't mount /tmp from rocquencourt_sesi.yaml
    ensure: absent
  swap:
    # Don't mount swap from rocquencourt_sesi.yaml
    ensure: absent
  # local mountpoints
  /srv/storage/space:
    ensure: absent
  /srv/softwareheritage/objects-xfs/0:
    ensure: absent
  /srv/softwareheritage/objects-xfs/1:
    ensure: absent
  /srv/softwareheritage/objects-xfs/2:
    ensure: absent
  /srv/softwareheritage/objects-xfs/3:
    ensure: absent
  /srv/softwareheritage/objects-xfs/4:
    ensure: absent
  /srv/softwareheritage/objects-xfs/5:
    ensure: absent
  /srv/softwareheritage/objects-xfs/6:
    ensure: absent
  /srv/softwareheritage/objects-xfs/7:
    ensure: absent
  /srv/softwareheritage/objects-xfs/8:
    ensure: absent
  /srv/softwareheritage/objects-xfs/9:
    ensure: absent
  /srv/softwareheritage/objects-xfs/a:
    ensure: absent
  /srv/softwareheritage/objects-xfs/b:
    ensure: absent
  /srv/softwareheritage/objects-xfs/c:
    ensure: absent
  /srv/softwareheritage/objects-xfs/d:
    ensure: absent
  /srv/softwareheritage/objects-xfs/e:
    ensure: absent
  /srv/softwareheritage/objects-xfs/f:
    ensure: absent
  /srv/storage/content-replayer:
    device: content-replayer-scratch
    fstype: tmpfs
    options:
      - nodev
      - nosuid
      - noexec
      - size=8G
      - uid=swhworker
      - gid=swhdev

swh::apt_config::backported_packages:
  buster:
    # Recent systemd makes saam unbootable!
    - -libnss-myhostname
    - -libnss-mymachines
    - -libnss-resolve
    - -libnss-systemd
    - -libpam-systemd
    - -libsystemd-dev
    - -libsystemd0
    - -libudev-dev
    - -libudev1
    - -libudev1-udeb
    - -libzstd1
    - -systemd
    - -systemd-container
    - -systemd-coredump
    - -systemd-journal-remote
    - -systemd-sysv
    - -systemd-tests
    - -udev
    - -udev-udeb
